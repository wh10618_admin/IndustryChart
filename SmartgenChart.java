package com.smartgen.chart;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.xiaowangye.chart.sgchart.MeterView;
import com.xiaowangye.chart.sgchart.SCBarChart;
import com.xiaowangye.chart.sgchart.VoltageRectangleView;
import com.xiaowangye.chart.sgchart.VoltageSquareView;


public class SmartgenChart extends AppCompatActivity {

    //柱形图
    private SCBarChart barChart,barChart2,barChart3;

    private MeterView meterView;

    private VoltageSquareView voltageView;

    private VoltageRectangleView voltageRectangleView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smartgen_chart);

        barChart = (SCBarChart) findViewById(R.id.barchart);
        barChart.setBaseType(SCBarChart.BaseType.rect).setScBorderWidth(6).setScName("水位1").setScUnit("%").setMaxScaleNum(6).setRightBorderIsShow(false).setRatedValue(80).setRatedValueHeight(5);


        barChart2 = (SCBarChart) findViewById(R.id.barchart2);
        barChart2.setBaseType(SCBarChart.BaseType.rect).setScBorderWidth(6).setScName("水位2").setScUnit("%").setMaxScaleNum(6).setRightBorderIsShow(false).setLeftBorderIsShow(false);


        barChart3 = (SCBarChart) findViewById(R.id.barchart3);
        barChart3.setBaseType(SCBarChart.BaseType.cycle).setScBorderWidth(6).setScName("温度").setScUnit("℃").setMaxScaleNum(6).setLeftBorderIsShow(false).setMinScaleNum(9);

        meterView = (MeterView) findViewById(R.id.meterview);
        meterView.setScBorderColor(0xFFCCCC33).setScBorderWidth(15).setScName("转速").setScUnit("r/min").setMaxScaleWidth(50).setMaxScaleHeight(10).setMaxValue(6000).setScValue(500).setMinScaleNum(9).setScNameFontSize(50).setScValueFontSize(100);

        voltageView = (VoltageSquareView) findViewById(R.id.voltageSquareView);
        voltageView.setScBorderColor(0xFFCCCC33).setScBorderWidth(15).setScName("市电Ua").setScUnit("V").setMaxScaleWidth(50).setMaxScaleHeight(10).setMaxValue(3000).setScValue(100).setMinScaleNum(9).setInnerBorderColor(0x5533FFCC).setBorderTopMargin(100).setScNameFontSize(50).setScValueFontSize(100);

        voltageRectangleView = (VoltageRectangleView) findViewById(R.id.voltageRectangleView);
        voltageRectangleView.setScBorderColor(0xFFCCCC33).setScBorderWidth(15).setScName("市电Ua").setScUnit("V").setMaxScaleWidth(50).setMaxScaleHeight(10).setMaxValue(3000).setScValue(1000).setMinScaleNum(9).setInnerDistence(190).setBorderTopMargin(150).setScNameFontSize(50).setScValueFontSize(100);


        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    int value = (int) (Math.random()*100);
                    barChart.refreshData(value);
                    value = (int) (Math.random()*100);
                    barChart2.refreshData(value);
                    value = (int) (Math.random()*100);
                    barChart3.refreshData(value);


                    value = (int) (Math.random()*6000);
                    meterView.refreshData(value);

                    value = (int) (Math.random()*3000);
                    voltageView.refreshData(value);

                    value = (int) (Math.random()*3000);
                    voltageRectangleView.refreshData(value);
                    try{
                        Thread.sleep(500*4);
                    }catch(Exception e){
                    }
                }
            }
        }).start();
        //
    }
}
