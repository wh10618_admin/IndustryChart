### # Android App IndustryChart
 
实现对转速、温度及水位等仪表的图形化展示

 
1. - sgchart.jar  核心Jar  
        其中包含
           
-            SCBarChart （水温、液位）  
-            MeterView（仪表盘）  
-            VoltageRectangleView （长方形电压表）  
-            VoltageSquareView  （正方形电压表）




2. - activity_smartgen_chart.xml  Demo 界面
3. - SmartgenChart                Demo 实现类



核心代码如下：
        


        barChart = (SCBarChart) findViewById(R.id.barchart);
        barChart.setBaseType(SCBarChart.BaseType.rect).setScBorderWidth(6).setScName("水位1")
                .setScUnit("%").setMaxScaleNum(6).setRightBorderIsShow(false).setRatedValue(80)
                .setRatedValueHeight(5);


        barChart2 = (SCBarChart) findViewById(R.id.barchart2);
        barChart2.setBaseType(SCBarChart.BaseType.rect).setScBorderWidth(6).setScName("水位2")
                 .setScUnit("%").setMaxScaleNum(6).setRightBorderIsShow(false).setLeftBorderIsShow(false);


        barChart3 = (SCBarChart) findViewById(R.id.barchart3);
        barChart3.setBaseType(SCBarChart.BaseType.cycle).setScBorderWidth(6).setScName("温度")
                 .setScUnit("℃").setMaxScaleNum(6).setLeftBorderIsShow(false).setMinScaleNum(9);

        meterView = (MeterView) findViewById(R.id.meterview);
        meterView.setScBorderColor(0xFFCCCC33).setScBorderWidth(15).setScName("转速")
                 .setScUnit("r/min").setMaxScaleWidth(50).setMaxScaleHeight(10).setMaxValue(6000)
                 .setScValue(500).setMinScaleNum(9).setScNameFontSize(50).setScValueFontSize(100);

        voltageView = (VoltageSquareView) findViewById(R.id.voltageSquareView);
        voltageView.setScBorderColor(0xFFCCCC33).setScBorderWidth(15).setScName("市电Ua")
                   .setScUnit("V").setMaxScaleWidth(50).setMaxScaleHeight(10).setMaxValue(3000)
                   .setScValue(100).setMinScaleNum(9).setInnerBorderColor(0x5533FFCC).setBorderTopMargin(100)
                   .setScNameFontSize(50).setScValueFontSize(100);

        voltageRectangleView = (VoltageRectangleView) findViewById(R.id.voltageRectangleView);
        voltageRectangleView.setScBorderColor(0xFFCCCC33).setScBorderWidth(15).setScName("市电Ua")
                            .setScUnit("V").setMaxScaleWidth(50).setMaxScaleHeight(10).setMaxValue(3000)
                            .setScValue(1000).setMinScaleNum(9).setInnerDistence(190).setBorderTopMargin(150)
                            .setScNameFontSize(50).setScValueFontSize(100);


详细说明正在整理中...


![输入图片说明](https://images.gitee.com/uploads/images/2020/0223/120219_0a3c250c_558273.png "微信图片_20200223115530.png")